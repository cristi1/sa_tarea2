const express = require("express");
const bodyParser = require('body-parser');
const app = express();
const cliente = require('request');
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

function solicitarEntrega(clienteActual,direccion){  
    let obj = {
        "cliente": clienteActual,
        "direccion": direccion
      };
  
      let options = {
        uri: 'http://localhost:8083/api/entrega-orden',
        method: 'POST',
        json: obj
      };
  
      cliente(options, (error, response)=>{
        if(!error && response.statusCode == 200){
          console.log("Ok repartidor!!");
        }
      });
  }

  module.exports = {
    "solicitarEntrega" : solicitarEntrega
}