const express = require("express");
const bodyParser = require('body-parser');
const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const ent = require('./entrega');

app.post('/recepcion-orden', function (req, res){
    let id = req.body.id;
    let cantidad = req.body.cantidad;
    let clienteActual = req.body.cliente;
    let direccion = req.body.direccion;

    console.log("id: " + id);
    console.log("cantidad: " + cantidad);
    console.log("cliente: " + clienteActual);
    console.log("direccion: " + direccion);

    ent.solicitarEntrega(clienteActual,direccion);

    res.send('OK');
});

module.exports = app;