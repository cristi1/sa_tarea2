const express = require("express");
const bodyParser = require('body-parser');
const app = express();
const cliente = require('request');
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const api = require('./src/recepcionOrden');
app.use('/api',api);

app.listen(8082, () => {
 console.log("[Restaurante] El servidor está inicializado en el puerto 8082");
});