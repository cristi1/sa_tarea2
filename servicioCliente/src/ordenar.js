const express = require("express");
const bodyParser = require('body-parser');
const app = express();
const cliente = require('request');
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

function ordenarPlatillo(){
    let id = 1;
    let cantidad = 2;
    let clienteActual = "Cristi Vásquez";
    let direccion = "zona 25";
  
    let obj = {
        "id": id,
        "cantidad": cantidad,
        "cliente": clienteActual,
        "direccion": direccion
      };
  
      let options = {
        uri: 'http://localhost:8082/api/recepcion-orden',
        method: 'POST',
        json: obj
      };
  
      cliente(options, (error, response)=>{
        if(!error && response.statusCode == 200){
          console.log("Ok restaurante!!");
        }
      });
  }

  module.exports = {
    "ordenarPlatillo" : ordenarPlatillo
}