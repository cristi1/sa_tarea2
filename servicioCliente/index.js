const express = require("express");
const bodyParser = require('body-parser');
const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const ent = require('./src/ordenar');

app.listen(8081, () => {
    ent.ordenarPlatillo();
 console.log("[Cliente] El servidor está inicializado en el puerto 8081");
});