const express = require("express");
const bodyParser = require('body-parser');
const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const api = require('./src/entregaOrden');
app.use('/api',api);

app.listen(8083, () => {
 console.log("[Repartidor] El servidor está inicializado en el puerto 8083");
});