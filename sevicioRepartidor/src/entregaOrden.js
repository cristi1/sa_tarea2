const express = require("express");
const bodyParser = require('body-parser');
const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.post('/entrega-orden', function (req, res){
    let clienteActual = req.body.cliente;
    let direccion = req.body.direccion;

    console.log("Orden entregar a : " + clienteActual);
    console.log("dirección de entrega: " + direccion);

    res.send('OK');
});

module.exports = app;